import 'package:clone_instagram/widget/story_widget.dart';
import 'package:clone_instagram/widget/user_post.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

class HomePage extends StatelessWidget {
  HomePage({super.key});

  final List user = [
    'dafa rofi',
    'vinny lindawaty',
    'mega widya',
    'febril',
    'pradian pasir koja',
    'hasan',
    'mario',
    'nesta',
    'bela'
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(
            'Instagram',
            style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),
          ),
          backgroundColor: Colors.transparent,
          elevation: 0,
          actions: const [
            Icon(
              Icons.add_box_outlined,
              size: 24,
              color: Colors.black,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 18),
              child: Icon(
                Icons.favorite_border_outlined,
                size: 24,
                color: Colors.black,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(right: 15),
              child: Icon(
                Icons.send_outlined,
                size: 24,
                color: Colors.black,
              ),
            ),
          ],
        ),
        body: Column(
          children: [
            SizedBox(
              height: 120,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  return story_widget(
                    name: user[index],
                  );
                },
                itemCount: user.length,
              ),
            ),
            const Divider(
              height: 1,
            ),
            user_post(name: 'febril'),
          ],
        ));
  }
}
