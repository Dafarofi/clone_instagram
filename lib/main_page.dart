import 'package:clone_instagram/pages/home_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

class main_page extends StatefulWidget {
  const main_page({super.key});

  @override
  State<main_page> createState() => _main_pageState();
}

class _main_pageState extends State<main_page> {
  int _SelectedIndex = 0;
  void _onTap(int index) {
    setState(() {
      _SelectedIndex = index;
    });
  }

  List<Widget> pages = [
    HomePage(),
    const Center(
      child: Text(
        'Search',
        style: TextStyle(fontSize: 50),
      ),
    ),
    const Center(
      child: Text(
        'Reels',
        style: TextStyle(fontSize: 50),
      ),
    ),
    const Center(
      child: Text(
        'Profil',
        style: TextStyle(fontSize: 50),
      ),
    ),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: pages[_SelectedIndex],
      bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          showSelectedLabels: false,
          showUnselectedLabels: false,
          iconSize: 25,
          selectedItemColor: Colors.blue,
          currentIndex: _SelectedIndex,
          onTap: _onTap,
          items: [
            BottomNavigationBarItem(
              icon:
                  Icon(_SelectedIndex == 0 ? Icons.home : Icons.home_outlined),
              label: 'home',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                  _SelectedIndex == 1 ? Icons.search : Icons.search_outlined),
              label: 'search',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                  _SelectedIndex == 2 ? Icons.add_box : Icons.add_box_outlined),
              label: 'reels',
            ),
            BottomNavigationBarItem(
              icon: Icon(_SelectedIndex == 3
                  ? Icons.account_circle
                  : Icons.account_circle_outlined),
              label: 'profil',
            ),
          ]),
    );
  }
}
